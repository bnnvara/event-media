# Upgrade to 2.0

Version 2.0 was created to allow transition of integer media IDs to UUIDs.

## Behavior surrounding media IDs
* Will deserialize any integer IDs in the JSON payload as strings.
* Will produce a string value when calling `getId()`; breaks BC, hence major version bump.
* The Media constructor will accept only strings, again breaking BC.

## Behavior surrounding "concepts" and brands
A "concept" could in theory be either a "brand/channel/series" (normalized as "brand"), or a "season". 
When the event would be produced, the publisher would check if the "concept" has a parent, and *replace 
both the concept ID and the brand ID* with those of the parent.

Because of this only "series" will be published and they will normally have a "brand" ID. Investigating
client code reveals that the concept ID in the event isn't used beyond superfluous Unit Tests.

Taking these facts into account, version 2.0 simplifies things by offering a "brand" instead of a "concept"
and renaming the "brandId" to simply "id". It will still accept "concept" in the payload when deserializing,
normalizing to said "brand" structure when needed. 

## Rollout

Source of these `Media*Event` can keep producing integer until all client projects are updated. 
Update client projects in the dependency tree leaves first, root last.

## Configuration

This is not a bundle, never was, so you always needed to do manual configuration. Before you already
needed to do something like this:

```yaml
jms_serializer:
    metadata:
        auto_detection: false
        directories:
            events:
                namespace_prefix: "BNNVARA\\Event\\Media\\Domain"
                path: "%kernel.project_dir%/vendor/bnnvara/event-media/src/Infrastructure/config/serializer"
```

Now you'll also need to add the subscriber to your container as a service with the right tag, like so:

```yaml
services:
    BNNVARA\Event\Media\Application\Serializer\BrandNormalizingSubscriber:
        tags: ['jms_serializer.event_subscriber']
```