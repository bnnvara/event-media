<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Infrastructure\Messenger\RabbitMq;

use BNNVARA\Event\Media\Domain\MediaDeletedEvent;
use BNNVARA\Event\Media\Domain\MediaUpsertedEvent;
use BNNVARA\Event\Media\Infrastructure\Messenger\RabbitMq\RoutingKeys;
use PHPUnit\Framework\TestCase;

class RoutingKeysTest extends TestCase
{
    /** @test */
    public function routingKeysAreSet(): void
    {
        $routingKeys = (new RoutingKeys())->getRoutingKeys();

        $this->assertIsArray($routingKeys);
        $this->assertCount(2, $routingKeys);

        $this->assertSame('capi.event.media_upserted', $routingKeys[MediaUpsertedEvent::class]);
        $this->assertSame('capi.event.media_deleted', $routingKeys[MediaDeletedEvent::class]);
    }
}
