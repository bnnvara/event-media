<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Theme;
use PHPUnit\Framework\TestCase;

class ThemeTest extends TestCase
{
    /** @test */
    public function themeCanBeCreated(): void
    {
        $uuid = '9d2dd668-2532-450f-b642-d5874e1cbd98';
        $theme = new Theme($uuid);
        $this->assertSame($uuid, $theme->getId());
    }
}
