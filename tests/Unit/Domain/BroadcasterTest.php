<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Broadcaster;
use PHPUnit\Framework\TestCase;

class BroadcasterTest extends TestCase
{
    /** @test */
    public function attributesSetInConstructorAreReturnedByGetters(): void
    {
        $broadcaster = new Broadcaster(
            1234,
            'name'
        );

        $this->assertEquals(1234, $broadcaster->getId());
        $this->assertEquals('name', $broadcaster->getName());
    }
}
