<?php

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\ChildMedia;

class ChildMediaTest extends AbstractMediaRelationTest
{
    protected string $className = ChildMedia::class;
}
