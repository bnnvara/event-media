<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Source;
use PHPUnit\Framework\TestCase;

class SourceTest extends TestCase
{
    /** @test */
    public function expectedValuesAreReturnedByGettersWhenOnlyMandatoryParametersArePassedToTheConstructor(): void
    {
        $source = new Source(
            'format',
            'url'
        );

        $this->assertEquals('format', $source->getFormat());
        $this->assertEquals('url', $source->getUrl());
        $this->assertNull($source->getPriority());
    }

    /** @test */
    public function expectedValuesAreReturnedByGettersWhenAllParametersArePassedToTheConstructor(): void
    {
        $source = new Source(
            'format',
            'url',
            1000
        );

        $this->assertEquals(1000, $source->getPriority());
    }
}
