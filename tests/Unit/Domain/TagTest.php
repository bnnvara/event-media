<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Tag;
use PHPUnit\Framework\TestCase;

class TagTest extends TestCase
{
    /** @test */
    public function attributesSetInConstructorAreReturnedByGetters(): void
    {
        $tag = new Tag(
            'name'
        );

        $this->assertEquals('name', $tag->getName());
    }
}
