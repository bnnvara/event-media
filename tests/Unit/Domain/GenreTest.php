<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Genre;
use PHPUnit\Framework\TestCase;

class GenreTest extends TestCase
{
    /** @test */
    public function attributesSetInConstructorAreReturnedByGetters(): void
    {
        $tag = new Genre(
            'name'
        );

        $this->assertEquals('name', $tag->getName());
    }
}
