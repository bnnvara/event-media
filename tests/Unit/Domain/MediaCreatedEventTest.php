<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Media;
use BNNVARA\Event\Media\Domain\MediaCreatedEvent;
use PHPUnit\Framework\TestCase;

class MediaCreatedEventTest extends TestCase
{
    /** @test  */
    public function aMediaCreatedEventCanBeCreated(): void
    {
        $event = new MediaCreatedEvent($this->getMockedMedia());

        $this->assertInstanceOf(MediaCreatedEvent::class, $event);
    }

    private function getMockedMedia(): Media
    {
        $media = $this->getMockBuilder(Media::class)->disableOriginalConstructor()->getMock();

        /** @var Media $media */
        return $media;
    }
}
