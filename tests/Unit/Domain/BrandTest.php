<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Brand;
use PHPUnit\Framework\TestCase;

class BrandTest extends TestCase
{
    /** @test */
    public function attributesSetInConstructorAreReturnedByGetters(): void
    {
        $brand = new Brand(
            'a45d8627-ebd8-4a73-a2aa-070219989e52',
            'name',
        );

        $this->assertEquals('a45d8627-ebd8-4a73-a2aa-070219989e52', $brand->getId());
        $this->assertEquals('name', $brand->getName());
    }
}
