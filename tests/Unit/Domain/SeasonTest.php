<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Season;
use PHPUnit\Framework\TestCase;

class SeasonTest extends TestCase
{
    /**
     * @test
     * @dataProvider seasonProvider
     */
    public function seasonCanBeCreated(
        int $id,
        string $name,
        ?string $pomsMid,
    ): void {

        $season = new Season(
            id: $id,
            name: $name,
            pomsMid: $pomsMid
        );

        $this->assertSame($id, $season->getid());
        $this->assertSame($name, $season->getName());
        $this->assertSame($pomsMid, $season->getPomsMid());
    }

    public function seasonProvider(): array
    {
        return [
            'Complete' => [
                'id' => 1,
                'name' => 'Vroege Vogels Seizoen 1',
                'pomsMid' => 'POMS_123456789',
            ],
            'Nulled values' => [
                'id' => 1,
                'name' => 'Vroege Vogels Seizoen 1',
                'pomsMid' => null,
            ]
        ];
    }
}
