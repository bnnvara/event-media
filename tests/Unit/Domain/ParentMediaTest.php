<?php

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\ParentMedia;

class ParentMediaTest extends AbstractMediaRelationTest
{
    protected string $className = ParentMedia::class;
}
