<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Person;
use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{
    /**
     * @test
     * @dataProvider personProvider
     */
    public function attributesSetInConstructorAreReturnedByGetters(
        int $id,
        string $name,
        string $firstName,
        string $lastName,
        string $displayName,
        string $slug
    ): void {
        $person = new Person(
            id: $id,
            name: $name,
            firstName: $firstName,
            lastName: $lastName,
            displayName: $displayName,
            slug: $slug,
        );

        $this->assertSame($id, $person->getId());
        $this->assertSame($name, $person->getName());
        $this->assertSame($firstName, $person->getFirstName());
        $this->assertSame($lastName, $person->getLastName());
        $this->assertSame($displayName, $person->getDisplayName());
        $this->assertSame($slug, $person->getSlug());
    }

    public function personProvider(): array
    {
        return [
            'Complete' => [
                'id' => 100,
                'name' => 'Michael Jackson',
                'firstName' => 'Michael',
                'lastName' => 'Jackson',
                'displayName' => 'King of Pop',
                'slug' => 'michael-jackson'
            ],
            'Partial' => [
                'id' => 100,
                'name' => 'Michael Jackson',
                'firstName' => '',
                'lastName' => '',
                'displayName' => '',
                'slug' => ''
            ]
        ];
    }
}
