<?php

namespace Tests\BNNVARA\Unit\Domain;

use PHPUnit\Framework\TestCase;

abstract class AbstractMediaRelationTest extends TestCase
{
    protected string $className;

    /** @test */
    public function mediaRelationCanBeCreated(): void
    {
        $id = 12345;

        $relation = new $this->className($id);

        $this->assertSame($id, $relation->getId());
    }
}
