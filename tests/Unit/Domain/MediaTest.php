<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Brand;
use BNNVARA\Event\Media\Domain\Broadcaster;
use BNNVARA\Event\Media\Domain\CallToAction;
use BNNVARA\Event\Media\Domain\Category;
use BNNVARA\Event\Media\Domain\ChildMedia;
use BNNVARA\Event\Media\Domain\Classification;
use BNNVARA\Event\Media\Domain\Crid;
use BNNVARA\Event\Media\Domain\Genre;
use BNNVARA\Event\Media\Domain\Image;
use BNNVARA\Event\Media\Domain\Media;
use BNNVARA\Event\Media\Domain\ParentMedia;
use BNNVARA\Event\Media\Domain\Person;
use BNNVARA\Event\Media\Domain\Season;
use BNNVARA\Event\Media\Domain\Series;
use BNNVARA\Event\Media\Domain\Source;
use BNNVARA\Event\Media\Domain\Subtitle;
use BNNVARA\Event\Media\Domain\Tag;
use BNNVARA\Event\Media\Domain\Theme;
use DateTime;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class MediaTest extends TestCase
{
    private const MEDIA_UUID = '0ed80cf1-aad8-4f31-9c97-fa69e426360e';

    /** @test */
    public function attributesSetInConstructorAreReturnedByGetters(): void
    {
        $series = new Series(
            id: 1,
            name: 'Vroege Vogels',
            preprChannelId: '25df400e-1d76-46fc-913c-839d3003eba6',
            pomsMid: 'POMS_123456789',
        );

        $season = new Season(
            id: 2,
            name: 'Vroege Vogels Seizoen 1',
            pomsMid: 'POMS_123456789',
        );

        $createdAt = new DateTimeImmutable();

        $parents = [new ParentMedia(5829), new ParentMedia(5280)];
        $children = [new ChildMedia(4248), new ChildMedia(1140)];

        $crids = [
            'crid://bnnvara.mema.media_item/crid_number_1',
            'crid://bnnvara.mema.media_item/crid_number_2',
        ];

        $media = new Media(
            id: self::MEDIA_UUID,
            title: 'title',
            mediaType: 'mediaType',
            avType: 'avType',
            slug: 'slug',
            onlineDate: new DateTime('01-01-2020'),
            sources: $this->getSources(),
            persons: $this->getPersons(),
            broadcasters: $this->getBroadcasters(),
            tags: $this->getTags(),
            categories: $this->getCategories(),
            genres: $this->getGenres(),
            classifications: $this->getClassifications(),
            enabled: true,
            subtitles: $this->getSubtitles(),
            themes: $this->getThemes(),
            description: 'description',
            offlineDate: new DateTime('01-01-2020'),
            brand: $this->getBrand(),
            image: $this->getImage(),
            duration: 1234,
            pomsMid: 'POMS_BV_15906771',
            timeslot: 'TENPMUNTILSIXAM',
            callToAction: $callToAction = new CallToAction('title', null),
            series: $series,
            season: $season,
            created: $createdAt,
            parents: $parents,
            children: $children,
            crids: $crids,
        );

        $this->assertSame(self::MEDIA_UUID, $media->getId());
        $this->assertSame('title', $media->getTitle());
        $this->assertSame('description', $media->getDescription());
        $this->assertSame('mediaType', $media->getMediaType());
        $this->assertSame('avType', $media->getAvType());
        $this->assertSame('slug', $media->getSlug());
        $this->assertEquals(new DateTime('01-01-2020'), $media->getOnlineDate());
        $this->assertSame($callToAction, $media->getCallToAction());

        $this->assertIsArray($media->getSources());
        $this->assertContainsOnlyInstancesOf(Source::class, $media->getSources());
        $this->assertCount(3, $media->getSources());

        $this->assertIsArray($media->getPersons());
        $this->assertContainsOnlyInstancesOf(Person::class, $media->getPersons());
        $this->assertCount(3, $media->getPersons());

        $this->assertIsArray($media->getBroadcasters());
        $this->assertContainsOnlyInstancesOf(Broadcaster::class, $media->getBroadcasters());
        $this->assertCount(3, $media->getBroadcasters());

        $this->assertIsArray($media->getTags());
        $this->assertContainsOnlyInstancesOf(Tag::class, $media->getTags());
        $this->assertCount(3, $media->getTags());

        $this->assertIsArray($media->getCategories());
        $this->assertContainsOnlyInstancesOf(Category::class, $media->getCategories());
        $this->assertCount(3, $media->getCategories());

        $this->assertIsArray($media->getGenres());
        $this->assertContainsOnlyInstancesOf(Genre::class, $media->getGenres());
        $this->assertCount(3, $media->getGenres());

        $this->assertIsArray($media->getSubtitles());
        $this->assertContainsOnlyInstancesOf(Subtitle::class, $media->getSubtitles());
        $this->assertCount(2, $media->getSubtitles());
        $this->assertTrue($media->isEnabled());
        $this->assertIsArray($media->getClassifications());
        $this->assertContainsOnlyInstancesOf(Classification::class, $media->getClassifications());
        $this->assertCount(2, $media->getClassifications());

        $this->assertIsArray($media->getThemes());
        $this->assertContainsOnlyInstancesOf(Theme::class, $media->getThemes());
        $this->assertCount(2, $media->getThemes());

        $this->assertEquals(new DateTime('01-01-2020'), $media->getOfflineDate());
        $this->assertInstanceOf(Brand::class, $media->getBrand());
        $this->assertInstanceOf(Image::class, $media->getImage());
        $this->assertSame(1234, $media->getDuration());
        $this->assertSame('POMS_BV_15906771', $media->getPomsMid());
        $this->assertSame('TENPMUNTILSIXAM', $media->getTimeslot());

        $this->assertSame($series, $media->getSeries());
        $this->assertSame($season, $media->getSeason());

        $this->assertSame($createdAt, $media->getCreated());

        $this->assertSame($parents, $media->getParents());
        $this->assertSame($children, $media->getChildren());

        $this->assertSame($crids, $media->getCrids());
    }

    /** @test */
    public function optionalItemsCanBeNull(): void
    {
        $createdAt = new DateTimeImmutable();

        $onlineDate = null;

        $media = new Media(
            id: self::MEDIA_UUID,
            title: 'title',
            mediaType: 'mediaType',
            avType: 'avType',
            slug: 'slug',
            onlineDate: $onlineDate,
            sources: [],
            persons: [],
            broadcasters: [],
            tags: [],
            categories: [],
            genres: [],
            classifications: [],
            enabled: false,
            created: $createdAt,
        );

        $this->assertSame(self::MEDIA_UUID, $media->getId());
        $this->assertSame('title', $media->getTitle());
        $this->assertNull($media->getDescription());
        $this->assertSame('mediaType', $media->getMediaType());
        $this->assertSame('avType', $media->getAvType());
        $this->assertSame('slug', $media->getSlug());
        $this->assertSame($onlineDate, $media->getOnlineDate());
        $this->assertCount(0, $media->getSources());
        $this->assertCount(0, $media->getPersons());
        $this->assertCount(0, $media->getBroadcasters());
        $this->assertCount(0, $media->getTags());
        $this->assertCount(0, $media->getCategories());
        $this->assertCount(0, $media->getGenres());
        $this->assertCount(0, $media->getSubtitles());
        $this->assertFalse($media->isEnabled());
        $this->assertCount(0, $media->getClassifications());

        $this->assertNull($media->getOfflineDate());
        $this->assertNull($media->getBrand());
        $this->assertNull($media->getImage());
        $this->assertNull($media->getDuration());
        $this->assertNull($media->getPomsMid());
        $this->assertNull($media->getTimeslot());

        $this->assertNull($media->getSeries());
        $this->assertNull($media->getSeason());

        $this->assertSame($createdAt, $media->getCreated());

        $this->assertSame([], $media->getParents());
        $this->assertSame([], $media->getChildren());

        $this->assertSame([], $media->getCrids());
    }

    private function getBrand(): Brand
    {
        $brand = $this->getMockBuilder(Brand::class)->disableOriginalConstructor()->getMock();

        /** @var Brand $brand */
        return $brand;
    }

    private function getImage(): Image
    {
        $image = $this->getMockBuilder(Image::class)->disableOriginalConstructor()->getMock();

        /** @var Image $image */
        return $image;
    }

    private function getSourceMock(): Source
    {
        $source = $this->getMockBuilder(Source::class)->disableOriginalConstructor()->getMock();

        /** @var Source $source */
        return $source;
    }

    private function getSources(): array
    {
        $sources = [];
        for ($i = 0; $i < 3; $i++) {
            $sources[] = $this->getSourceMock();
        }
        /** @var Source[] $sources */
        return $sources;
    }

    private function getPersonMock(): Person
    {
        $person = $this->getMockBuilder(Person::class)->disableOriginalConstructor()->getMock();

        /** @var Person $person */
        return $person;
    }

    private function getPersons(): array
    {
        $persons = [];
        for ($i = 0; $i < 3; $i++) {
            $persons[] = $this->getPersonMock();
        }
        /** @var Person[] $persons */
        return $persons;
    }

    private function getBroadcasterMock(): Broadcaster
    {
        $broadcaster = $this->getMockBuilder(Broadcaster::class)->disableOriginalConstructor()->getMock();

        /** @var Broadcaster $broadcaster */
        return $broadcaster;
    }

    private function getBroadcasters(): array
    {
        $broadcasters = [];
        for ($i = 0; $i < 3; $i++) {
            $broadcasters[] = $this->getBroadcasterMock();
        }
        /** @var Broadcaster[] $broadcasters */
        return $broadcasters;
    }

    private function getCategoryMock(): Category
    {
        $category = $this->getMockBuilder(Category::class)->disableOriginalConstructor()->getMock();

        /** @var Category $category */
        return $category;
    }

    private function getCategories(): array
    {
        $categories = [];
        for ($i = 0; $i < 3; $i++) {
            $categories[] = $this->getCategoryMock();
        }
        /** @var Category[] $categories */
        return $categories;
    }

    private function getGenreMock(): Genre
    {
        $genre = $this->getMockBuilder(Genre::class)->disableOriginalConstructor()->getMock();

        /** @var Genre $genre */
        return $genre;
    }

    private function getGenres(): array
    {
        $genres = [];
        for ($i = 0; $i < 3; $i++) {
            $genres[] = $this->getGenreMock();
        }
        /** @var Genre[] $genres */
        return $genres;
    }

    private function getClassifications(): array
    {
        return [
            new Classification(
                '6 jaar en ouder',
                'age-6-and-up',
                'https://d1q2kgz2laecra.cloudfront.net/media/images/2019/04/26/6_jaar.png'
            ),
            new Classification(
                'Geweld',
                'violence',
                'https://d1q2kgz2laecra.cloudfront.net/media/images/2019/04/26/geweld.png'
            ),
        ];
    }

    private function getTagMock(): Tag
    {
        $tag = $this->getMockBuilder(Tag::class)->disableOriginalConstructor()->getMock();

        /** @var Tag $tag */
        return $tag;
    }

    private function getTags(): array
    {
        $tags = [];
        for ($i = 0; $i < 3; $i++) {
            $tags[] = $this->getTagMock();
        }
        /** @var Tag[] $tags */
        return $tags;
    }

    private function getSubtitles(): array
    {
        return [
            new Subtitle(
                'nl',
                'website'
            ),
            new Subtitle(
                'en',
                'website'
            ),
        ];
    }

    private function getThemes(): array
    {
        return [
            new Theme('099056c2-9908-4773-9748-e22e2d5e9c0c'),
            new Theme('1d9be58b-215f-46a5-974c-8a4a803a7fc1'),
        ];
    }
}
