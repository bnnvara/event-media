<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Subtitle;
use PHPUnit\Framework\TestCase;

class SubtitleTest extends TestCase
{
    /** @test */
    public function subtitleCanBeConstructedAndArgumentsCanBeRetrieved(): void
    {
        $subtitle = new Subtitle('name', 'source');

        $this->assertEquals('name', $subtitle->getLanguage());
        $this->assertEquals('source', $subtitle->getSource());
    }
}
