<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Image;
use PHPUnit\Framework\TestCase;

class ImageTest extends TestCase
{
    /** @test */
    public function attributesSetInConstructorAreReturnedByGetters(): void
    {
        $image = new Image(
            'url',
            'title'
        );

        $this->assertEquals('url', $image->getUrl());
        $this->assertEquals('title', $image->getTitle());
    }

    /** @test */
    public function optionalItemsCanBeNull(): void
    {
        $image = new Image(
            'url',
            null
        );

        $this->assertEquals('url', $image->getUrl());
        $this->assertNull($image->getTitle());
    }
}
