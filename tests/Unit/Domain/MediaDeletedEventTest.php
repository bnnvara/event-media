<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Media;
use BNNVARA\Event\Media\Domain\MediaDeletedEvent;
use PHPUnit\Framework\TestCase;

class MediaDeletedEventTest extends TestCase
{
    /** @test */
    public function aMediaDeletedEventCanBeCreated(): void
    {
        $media = $this->getMediaMock();
        $event = new MediaDeletedEvent($media);

        $this->assertEquals($this->getMediaMock(), $event->getData());
    }

    private function getMediaMock(): Media
    {
        $mediaMock = $this->getMockBuilder(Media::class)->disableOriginalConstructor()->getMock();

        /** @var Media $mediaMock */
        return $mediaMock;
    }
}
