<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Category;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    /** @test */
    public function attributesSetInConstructorAreReturnedByGetters(): void
    {
        $tag = new Category(
            1234,
            'name',
            'slug'
        );

        $this->assertEquals(1234, $tag->getId());
        $this->assertEquals('name', $tag->getName());
        $this->assertEquals('slug', $tag->getSlug());
    }
}
