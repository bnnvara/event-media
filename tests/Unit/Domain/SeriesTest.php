<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Series;
use PHPUnit\Framework\TestCase;

class SeriesTest extends TestCase
{
    /**
     * @test
     * @dataProvider seriesProvider
     */
    public function seriesCanBeCreated(
        int $id,
        string $name,
        ?string $preprChannelId,
        ?string $pomsMid,
    ): void {

        $series = new Series(
            id: $id,
            name: $name,
            preprChannelId: $preprChannelId,
            pomsMid: $pomsMid,
        );

        $this->assertSame($id, $series->getid());
        $this->assertSame($name, $series->getName());
        $this->assertSame($preprChannelId, $series->getPreprChannelId());
        $this->assertSame($pomsMid, $series->getPomsMid());
    }

    public function seriesProvider(): array
    {
        return [
            'Complete' => [
                'id' => 1,
                'name' => 'Vroege Vogels',
                'preprChannelId' => '25df400e-1d76-46fc-913c-839d3003eba6',
                'pomsMid' => 'POMS_123456789',
            ],
            'Nulled values' => [
                'id' => 1,
                'name`' => 'Vroege Vogels',
                'preprChannelId' => null,
                'pomsMid' => null,
            ]
        ];
    }
}
