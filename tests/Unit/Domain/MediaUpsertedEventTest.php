<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Unit\Domain;

use BNNVARA\Event\Media\Domain\Media;
use BNNVARA\Event\Media\Domain\MediaUpsertedEvent;
use PHPUnit\Framework\TestCase;

class MediaUpsertedEventTest extends TestCase
{
    /** @test */
    public function aMediaDeletedEventCanBeCreated(): void
    {
        $media = $this->getMediaMock();
        $event = new MediaUpsertedEvent($media);

        $this->assertEquals($this->getMediaMock(), $event->getData());
    }

    private function getMediaMock(): Media
    {
        $mediaMock = $this->getMockBuilder(Media::class)->disableOriginalConstructor()->getMock();

        /** @var Media $mediaMock */
        return $mediaMock;
    }
}
