<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Integration;

use BNNVARA\Event\Media\Domain\Brand;
use BNNVARA\Event\Media\Domain\Image;
use BNNVARA\Event\Media\Domain\Media;
use BNNVARA\Event\Media\Domain\MediaUpsertedEvent;
use PHPUnit\Framework\TestCase;

class MediaUpsertedEventDeserializeTest extends TestCase
{
    use SerializerTrait;

    private const MEDIA_UUID = '23e5ec0c-e538-4ddd-a4de-e9d482527aba';

    /** @test */
    public function eventCanBeDeserialized(): void
    {
        $serializer = $this->createSerializer();

        $event = $serializer->deserialize(
            file_get_contents(__DIR__ . '/MediaUpsertedEventDeserializeTest/event.json'),
            MediaUpsertedEvent::class,
            'json'
        );

        /** @var Media $media */
        $media = $event->getData();

        $this->assertSame(self::MEDIA_UUID, $media->getId());
        $this->assertInstanceOf(Brand::class, $media->getBrand());
        $this->assertSame('De Beste Singer Songwriter van Nederland', $media->getBrand()->getName());
        $this->assertSame('df6a438c-0932-4e97-b437-d40b0d895e0e', $media->getBrand()->getId());
        $this->assertSame('Description', $media->getDescription());
        $this->assertCount(1, $media->getGenres());
        $this->assertSame('Muziek', $media->getGenres()[0]->getName());
        $this->assertInstanceOf(Image::class, $media->getImage());
        $this->assertSame('Titel', $media->getImage()->getTitle());
        $this->assertSame('https://media.vara.nl/images/logos/dbss_UG.jpg', $media->getImage()->getUrl());
        $this->assertSame('CLIP', $media->getMediaType());
        $this->assertSame('VIDEO', $media->getAvType());
        $this->assertSame('2012-07-16T19:33:17+00:00', $media->getOnlineDate()->format('c'));
        $this->assertSame('2013-07-16T19:33:17+00:00', $media->getOfflineDate()->format('c'));
        $this->assertCount(2, $media->getPersons());
        $this->assertSame('Jan Janssen', $media->getPersons()[0]->getName());
        $this->assertSame('Piet Pietersen', $media->getPersons()[1]->getName());
        $this->assertSame('de-beste-singer-songwriter-van-nederland-123456', $media->getSlug());
        $this->assertCount(1, $media->getBroadcasters());
        $this->assertSame(2, $media->getBroadcasters()[0]->getId());
        $this->assertSame('VARA', $media->getBroadcasters()[0]->getName());
        $this->assertSame('De beste singer-songwriter van Nederland', $media->getTitle());

        $this->assertTrue($media->getPublishToPoms());
        $this->assertIsArray($media->getSubtitles());
        $this->assertCount(1, $media->getSubtitles());
        $this->assertSame('name', $media->getSubtitles()[0]->getLanguage());
        $this->assertSame('source', $media->getSubtitles()[0]->getSource());

        $this->assertCount(2, $media->getTags());
        $this->assertSame('muziek', $media->getTags()[0]->getName());
        $this->assertSame('amusement', $media->getTags()[1]->getName());
        $this->assertCount(1, $media->getCategories());
        $this->assertSame(1, $media->getCategories()[0]->getId());
        $this->assertSame('categorie 1', $media->getCategories()[0]->getName());
        $this->assertSame('categorie-1', $media->getCategories()[0]->getSlug());
        $this->assertCount(1, $media->getSources());
        $this->assertSame('UNKNOWN', $media->getSources()[0]->getFormat());
        $this->assertSame('http://player.omroep.nl/?aflID=14613978', $media->getSources()[0]->getUrl());
        $this->assertSame(0, $media->getSources()[0]->getPriority());
        $this->assertSame('poms wat?', $media->getPomsMid());
        $this->assertCount(1, $media->getClassifications());
        $this->assertSame('type', $media->getClassifications()[0]->getType());
        $this->assertSame('title', $media->getClassifications()[0]->getTitle());
        $this->assertSame('url', $media->getClassifications()[0]->getImageUrl());
        $this->assertSame('timeslot', $media->getTimeslot());
        $this->assertSame(60, $media->getDuration());

        $this->assertSame('title of cta', $media->getCallToAction()->getTitle());
        $this->assertSame('url of cta', $media->getCallToAction()->getUrl());

        $this->assertCount(1, $media->getThemes());
        $this->assertSame('1d9be58b-215f-46a5-974c-8a4a803a7fc1', $media->getThemes()[0]->getId());
    }

    /** @test */
    public function eventCanBeDeserializedIfOptionalFieldsAreNotSet(): void
    {
        /** @var MediaUpsertedEvent $event */
        $event = $this->createSerializer()->deserialize(
            file_get_contents(__DIR__ . '/MediaUpsertedEventDeserializeTest/event-without-optional-fields.json'),
            MediaUpsertedEvent::class,
            'json'
        );

        $media = $event->getData();

        $this->assertNull($media->getBrand());
        $this->assertNull($media->getDescription());
        $this->assertNull($media->getImage());
        $this->assertNull($media->getOfflineDate());
        $this->assertEmpty($media->getSubtitles());
        $this->assertNull($media->getDuration());
        $this->assertNull($media->getPomsMid());
        $this->assertNull($media->getTimeslot());
        $this->assertIsArray($media->getSubtitles());
        $this->assertIsArray($media->getThemes());
    }
}
