<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Integration;

use BNNVARA\Event\Media\Domain\MediaUpsertedEvent;
use PHPUnit\Framework\TestCase;

class MediaIdNormalizingSubscriberTest extends TestCase
{
    use SerializerTrait;

    /** @test */
    public function integerIDWillBeDeserializedAsString(): void
    {
        $event = $this->createSerializer()->deserialize(
            file_get_contents(__DIR__ . '/MediaIdNormalizingSubscriberTest/event.json'),
            MediaUpsertedEvent::class,
            'json'
        );

        $data = $event->getData();
        $this->assertSame("9878907", $data->getId());
    }
}
