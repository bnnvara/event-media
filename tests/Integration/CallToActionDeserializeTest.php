<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Integration;

use BNNVARA\Event\Media\Domain\CallToAction;
use PHPUnit\Framework\TestCase;

class CallToActionDeserializeTest extends TestCase
{
    use SerializerTrait;

    /** @test */
    public function callToActionWithUrlCanBeDeserialized(): void
    {
        /** @var CallToAction $callToAction */
        $callToAction = $this->createSerializer()
            ->deserialize('{"title": "title", "url": "url"}', CallToAction::class, 'json');

        $this->assertInstanceOf(
            CallToAction::class,
            $callToAction
        );

        $this->assertSame('title', $callToAction->getTitle());
        $this->assertSame('url', $callToAction->getUrl());
    }

    /** @test */
    public function callToActionWithoutNullUrlCanBeDeserialized(): void
    {
        /** @var CallToAction $callToAction */
        $callToAction = $this->createSerializer()
            ->deserialize('{"title": "title", "url": null}', CallToAction::class, 'json');

        $this->assertInstanceOf(
            CallToAction::class,
            $callToAction
        );

        $this->assertSame('title', $callToAction->getTitle());
        $this->assertNull($callToAction->getUrl());
    }

    /** @test */
    public function callToActionWithoutUrlCanBeDeserialized(): void
    {
        /** @var CallToAction $callToAction */
        $callToAction = $this->createSerializer()
            ->deserialize('{"title": "title"}', CallToAction::class, 'json');

        $this->assertInstanceOf(
            CallToAction::class,
            $callToAction
        );

        $this->assertSame('title', $callToAction->getTitle());
        $this->assertNull($callToAction->getUrl());
    }
}
