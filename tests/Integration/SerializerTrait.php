<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Integration;

use JMS\Serializer\EventDispatcher\EventDispatcher;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;

trait SerializerTrait
{
    protected function createSerializer(EventSubscriberInterface ...$subscribers): SerializerInterface
    {
        return SerializerBuilder::create()
            ->addMetadataDir(__DIR__ . '/../../src/Infrastructure/config/serializer', 'BNNVARA\Event\Media\Domain')
            ->setDebug(true)
            ->configureListeners(
                function (EventDispatcher $dispatcher) use ($subscribers): void {
                    foreach ($subscribers as $subscriber) {
                        $dispatcher->addSubscriber($subscriber);
                    }
                }
            )
            ->build();
    }
}
