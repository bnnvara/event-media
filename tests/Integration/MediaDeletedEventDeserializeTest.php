<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Integration;

use BNNVARA\Event\Media\Domain\Media;
use BNNVARA\Event\Media\Domain\MediaDeletedEvent;
use DateTime;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class MediaDeletedEventDeserializeTest extends TestCase
{
    use SerializerTrait;

    private const MEDIA_UUID = 'fab2a1b5-cebe-4ed5-9305-5e8a7f32ca81';

    /** @test */
    public function aMediaDeletedEventCanBeSerialized(): void
    {
        $serializer = $this->createSerializer();

        $media = $this->getMedia();

        $mediaJson = $serializer->serialize(
            new MediaDeletedEvent($media),
            'json'
        );

        $expectedJson = file_get_contents(__DIR__ . '/MediaDeletedEventDeserializeTest/minimal-media-deleted-event.json');

        $expectedJson = strtr(
            $expectedJson,
            [
                '{createdAt}' => $media->getCreated()->format(DateTimeImmutable::RFC3339)
            ]
        );

        $this->assertJsonStringEqualsJsonString(
            $expectedJson,
            $mediaJson
        );
    }

    /** @test */
    public function aMediaDeletedEventCanBeDeserialized(): void
    {
        $expectedMedia = $this->getMedia();

        $json = file_get_contents(
            __DIR__ . '/MediaDeletedEventDeserializeTest/minimal-media-deleted-event.json'
        );

        $json = strtr(
            $json,
            [
                '{createdAt}' => $expectedMedia->getCreated()->format(DateTimeImmutable::RFC3339)
            ]
        );

        /** @var MediaDeletedEvent $mediaDeletedEvent */
        $mediaDeletedEvent = $this->createSerializer()->deserialize(
            $json,
            MediaDeletedEvent::class,
            'json'
        );

        $this->assertEquals($mediaDeletedEvent->getData()->getId(), $expectedMedia->getId());
        $this->assertEquals($mediaDeletedEvent->getData()->getMediaType(), $expectedMedia->getMediaType());
        $this->assertEquals($mediaDeletedEvent->getData()->getOnlineDate(), $expectedMedia->getOnlineDate());
        $this->assertEquals($mediaDeletedEvent->getData()->getSlug(), $expectedMedia->getSlug());
        $this->assertEquals($mediaDeletedEvent->getData()->getAvType(), $expectedMedia->getAvType());
        $this->assertEquals($mediaDeletedEvent->getData()->getTitle(), $expectedMedia->getTitle());
        $this->assertEquals($mediaDeletedEvent->getData()->getCreated(), $expectedMedia->getCreated());
    }

    private function getMedia(): Media
    {
        return new Media(
            id: self::MEDIA_UUID,
            title: 'title',
            mediaType: 'media type',
            avType: 'video',
            slug: 'slug',
            onlineDate: new DateTime('01-01-2020T10:00:00+00:00'),
            sources: [],
            persons: [],
            broadcasters: [],
            tags: [],
            categories: [],
            genres: [],
            classifications: [],
            enabled: true,
            created: new DateTimeImmutable('yesterday')
        );
    }
}
