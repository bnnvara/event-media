#!/bin/bash

# exit when any command fails
set -eu

composer config --global --auth http-basic.repo.packagist.com token $PACKAGIST_TOKEN

composer install --no-scripts

XDEBUG_MODE=coverage vendor/bin/phpunit -c phpunit.xml.dist --stop-on-failure --fail-on-empty-test-suite --log-junit ./var/reports/phpunit.junit.xml --coverage-clover=./var/reports/phpunit.coverage.xml