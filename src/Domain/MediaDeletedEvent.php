<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Domain;

class MediaDeletedEvent
{
    public function __construct(private Media $data)
    {
    }

    public function getData(): Media
    {
        return $this->data;
    }
}
