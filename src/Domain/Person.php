<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Domain;

class Person
{
    public function __construct(
        private int $id,
        private string $name,
        private string $firstName,
        private string $lastName,
        private string $displayName,
        private string $slug,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }
}
