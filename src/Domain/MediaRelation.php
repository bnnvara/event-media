<?php

namespace BNNVARA\Event\Media\Domain;

abstract class MediaRelation
{
    public function __construct(
        private int $id
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }
}
