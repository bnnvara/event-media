<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Domain;

class Image
{
    private ?string $title = null;

    public function __construct(
        private string $url,
        ?string $title = null
    ) {
        $this->title = $title;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }
}
