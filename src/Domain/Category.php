<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Domain;

class Category
{
    public function __construct(
        private int $id,
        private string $name,
        private string $slug
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }
}
