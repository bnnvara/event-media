<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Domain;

class Genre
{
    public function __construct(private string $name)
    {
    }

    public function getName(): string
    {
        return $this->name;
    }
}
