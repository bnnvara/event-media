<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Domain;

class Broadcaster
{
    public function __construct(
        private int $id,
        private string $name
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
