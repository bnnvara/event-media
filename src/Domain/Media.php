<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Domain;

use DateTime;
use DateTimeImmutable;
use Symfony\Component\Serializer\Annotation\SerializedName;

class Media
{
    private ?string $description = null;
    private ?DateTime $offlineDate = null;
    private ?Brand $brand = null;
    private ?Image $image = null;
    private ?int $duration = null;
    private ?string $pomsMid = null;
    private ?string $timeslot = null;
    private bool $publishToPoms = false;
    private ?CallToAction $callToAction = null;
    private array $subtitles = [];
    private array $themes = [];

    private array $parents = [];

    private array $children = [];

    public function __construct(
        private string $id,
        private string $title,
        private string $mediaType,
        private string $avType,
        private string $slug,
        private ?DateTime $onlineDate,
        private array $sources,
        private array $persons,
        private array $broadcasters,
        private array $tags,
        private array $categories,
        private array $genres,
        private array $classifications,
        private bool $enabled,
        private DateTimeImmutable $created,
        array $subtitles = [],
        array $themes = [],
        array $parents = [],
        array $children = [],
        bool $publishToPoms = false,
        ?string $description = null,
        ?DateTime $offlineDate = null,
        ?Brand $brand = null,
        ?Image $image = null,
        ?int $duration = null,
        ?string $pomsMid = null,
        ?string $timeslot = null,
        ?CallToAction $callToAction = null,
        private ?Series $series = null,
        private ?Season $season = null,
        private array $crids = [],
    ) {
        $this->subtitles = $subtitles;
        $this->callToAction = $callToAction;
        $this->timeslot = $timeslot;
        $this->pomsMid = $pomsMid;
        $this->duration = $duration;
        $this->image = $image;
        $this->brand = $brand;
        $this->offlineDate = $offlineDate;
        $this->description = $description;
        $this->publishToPoms = $publishToPoms;
        $this->themes = $themes;
        $this->parents = $parents;
        $this->children = $children;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getMediaType(): string
    {
        return $this->mediaType;
    }

    public function getAvType(): string
    {
        return $this->avType;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getPublishToPoms(): bool
    {
        return $this->publishToPoms;
    }

    public function getOnlineDate(): ?DateTime
    {
        return $this->onlineDate;
    }

    /**
     * @return Source[]
     */
    public function getSources(): array
    {
        return $this->sources;
    }

    /**
     * @return Person[]
     */
    public function getPersons(): array
    {
        return $this->persons;
    }

    /**
     * @return Broadcaster[]
     */
    public function getBroadcasters(): array
    {
        return $this->broadcasters;
    }

    /**
     * @return Tag[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @return Category[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @return Genre[]
     */
    public function getGenres(): array
    {
        return $this->genres;
    }

    public function getOfflineDate(): ?DateTime
    {
        return $this->offlineDate;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    /**
     * @return Subtitle[]
     */
    public function getSubtitles(): array
    {
        return $this->subtitles;
    }

    /**
     * @return Theme[]
     */
    public function getThemes(): array
    {
        return $this->themes;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function getPomsMid(): ?string
    {
        return $this->pomsMid;
    }

    /**
     * @return Classification[]
     */
    public function getClassifications(): array
    {
        return $this->classifications;
    }

    public function getTimeslot(): ?string
    {
        return $this->timeslot;
    }

    public function getCallToAction(): ?CallToAction
    {
        return $this->callToAction;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function getSeries(): ?Series
    {
        return $this->series;
    }

    public function getSeason(): ?Season
    {
        return $this->season;
    }

    public function getCreated(): DateTimeImmutable
    {
        return $this->created;
    }

    /**
     * @return ParentMedia[]
     */
    public function getParents(): array
    {
        return $this->parents;
    }

    /**
     * @return ChildMedia[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @return string[]
     */
    public function getCrids(): array
    {
        return $this->crids;
    }
}
