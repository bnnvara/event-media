<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Domain;

class Series
{
    public function __construct(
        private int $id,
        private string $name,
        private ?string $preprChannelId,
        private ?string $pomsMid,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPreprChannelId(): ?string
    {
        return $this->preprChannelId;
    }

    public function getPomsMid(): ?string
    {
        return $this->pomsMid;
    }
}
