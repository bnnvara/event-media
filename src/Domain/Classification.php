<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Domain;

class Classification
{
    public function __construct(
        private string $title,
        private string $type,
        private string $imageUrl
    ) {
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }
}
