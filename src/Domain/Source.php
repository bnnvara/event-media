<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Domain;

class Source
{
    private ?int $priority = null;

    public function __construct(
        private string $format,
        private string $url,
        ?int $priority = null
    ) {
        $this->priority = $priority;
    }

    public function getFormat(): string
    {
        return $this->format;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }
}
