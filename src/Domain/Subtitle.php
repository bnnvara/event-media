<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Domain;

class Subtitle
{
    public function __construct(
        private string $language,
        private string $source
    ) {
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function getSource(): string
    {
        return $this->source;
    }
}
