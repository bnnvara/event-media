<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Domain;

class Brand
{
    public function __construct(
        private string $id,
        private string $name,
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
