<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Domain;

class CallToAction
{
    private ?string $url = null;

    public function __construct(
        private string $title,
        ?string $url = null
    ) {
        $this->url = $url;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }
}
