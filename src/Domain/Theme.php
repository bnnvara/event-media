<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Domain;

class Theme
{
    public function __construct(
        private string $id
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }
}
