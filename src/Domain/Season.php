<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Domain;

class Season
{
    public function __construct(
        private int $id,
        private string $name,
        private ?string $pomsMid,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPomsMid(): ?string
    {
        return $this->pomsMid;
    }
}
