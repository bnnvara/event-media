<?php

declare(strict_types=1);

namespace BNNVARA\Event\Media\Infrastructure\Messenger\RabbitMq;

use BNNVARA\Event\Media\Domain\MediaDeletedEvent;
use BNNVARA\Event\Media\Domain\MediaUpsertedEvent;
use BNNVARA\MessengerBundle\Messenger\RoutingKey\RoutingKeyCollectionInterface;

class RoutingKeys implements RoutingKeyCollectionInterface
{
    private const ROUTING_KEYS = [
        MediaUpsertedEvent::class => 'capi.event.media_upserted',
        MediaDeletedEvent::class => 'capi.event.media_deleted',
    ];

    public function getRoutingKeys(): array
    {
        return self::ROUTING_KEYS;
    }
}
